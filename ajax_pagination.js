// Сайт с аякс пагинацией
const fs = require('fs');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const puppeteer = require('puppeteer');
const request = require('request-promise');


async function run() {
  let data = [];
  const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info', 'brand'];
  const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
  const page = await browser.newPage();
  const link = 'http://www.mims.ru/ru-RU/about/exhibitor-list/exhibitor-list-2013.aspx';
  await page.goto(link);

  let content, $;
  content = await page.content();
  $ = cheerio.load(content);


  let pageLinks = [];
  $('em', '.exhibitorslist').each(function () {
    const link = $(this).find('a.name').first().attr('href');
    pageLinks.push(link);
  });
  console.log(pageLinks.length);
  pageLinks = pageLinks.filter(link => link);

  const t = await main(pageLinks);
  data = data.concat(t);

  const pagerCount = $('a', '.pager');

  for (let i = 0; i < 62; i++) {
    pageLinks = [];
    await page.click('.pager > span + a');
    // await page.waitForNavigation();
    await page.waitFor(3500);
    content = await page.content();
    $ = cheerio.load(content);

    $('em', '.exhibitorslist').each(function () {
      const link = $(this).find('a.name').first().attr('href');
      pageLinks.push(link);
    });
    console.log('asdasd', pageLinks.length);
    pageLinks = pageLinks.filter(link => link);

    const w = await main(pageLinks);
    data = data.concat(w);
    console.log(data.length);
  }

  console.log('end');

  const csv = json2csv({ data, fields });
  const fileName = 'mims-2013.csv';
  fs.writeFile(fileName, csv, (err) => {
    if (err) {
      return console.log(err);
    };
    console.log('file saved');
    browser.close();
  });


}

run();

async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = await getRecord(links[i]);
      promises.push(record);
    }
    const d = await Promise.all(promises);
    return d;

  } catch (err) {
    console.log('error', err);
  }
}

function getRecord(link) {
  const options = {
    uri: `http://www.mims.ru${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let name, address, phone, site, fax, info, email, brand;
      // name = $('h2', '.scorecard').first().text().trim();
      // address = $('div', '.scorecard').eq(1).find('p').text().trim();
      // phone = $('div', '.scorecard').eq(3).find('div').find('p').text().trim();
      // fax = $('div', '.scorecard').eq(5).find('div').find('p').text().trim();
      // site = $('div', '.scorecard').eq(7).find('div').find('a').attr('href');
      // email = $('div', '.scorecard').eq(9).find('div').find('a').text().trim();
      // info = $('div', '.scorecard').eq(11).find('div').find('p').text().trim();
      // const brandEl = $('div', '.scorecard').eq(13).find('p').get(0);
      // brand = brandEl ? brandEl.next.data : '';

      name = $('.admin_add').find('h2').first().text().trim();
      $('div', '.admin_add').each(function () {
        const strong = $(this).find('strong');
        if (strong.length && strong.text() === 'Адрес') {
          address = $(this).contents().eq(2).text().trim();
        }
        if (strong.length && strong.text() === 'Номера телефона') {
          phone = $(this).contents().eq(2).text().trim();
        }
        if (strong.length && strong.text() === 'Номер факса') {
          fax = $(this).contents().eq(2).text().trim();
        }
        if (strong.length && strong.text() === 'Сайт') {
          site = $(this).contents().eq(3).attr('href');
        }
        if (strong.length && strong.text() === 'Описание компании') {
          info = $(this).contents().eq(2).text().trim();
        }
        if (strong.length && strong.text() === 'Бренды') {
          brand = $(this).contents().eq(1).text().trim();
        }
      });

      // name = $('.admin_add').find('h2').first().text().trim();
      // $('div.name', '.exhibitorview').each(function () {
      //   if ($(this).find('strong').text() === 'Адрес') {
      //     address = $(this).next().text().trim();
      //   }
      //   if ($(this).find('strong').text() === 'Номера телефона') {
      //     phone = $(this).next().text().trim();
      //   }
      //   if ($(this).find('strong').text() === 'Номер факса') {
      //     fax = $(this).next().text().trim();
      //   }
      //   if ($(this).find('strong').text() === 'Сайт') {
      //     site = $(this).next().find('a').attr('href');
      //   }
      //   if ($(this).find('strong').text() === 'Email') {
      //     email = $(this).next().find('a').text();
      //   }
      //   if ($(this).find('strong').text() === 'Описание компании') {
      //     info = $(this).next().text().trim();
      //   }
      //   if ($(this).find('strong').text() === 'Бренды') {
      //     brand = $(this).get(0).next.data.trim();
      //   }
      // });
      

         
      const s = { name, address, phone, fax, email, site, info, brand };
      // console.log(s);
      return { name, address, phone, fax, email, site, info, brand };
    })
    .catch(error => {
      console.log('error', error);
    });
}