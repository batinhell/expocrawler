const fs = require('fs');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const puppeteer = require('puppeteer');
const request = require('request-promise');

let browser;
async function run() {
  let data = [];
  const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'];

  browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  const link = 'https://pirexpo.com/exposition/companies/alphabet=all';
  await page.goto(link, {timeout: 1000000});
  let content, $;
  await page.waitForSelector('.card_company');
  content = await page.content();
  $ = cheerio.load(content);
  

  // function timeout(ms, el) {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(async function() {
  //       let name, address, phone, site, fax, info, email, selector;
  //       await el.click();
  //       await page.waitFor(500);
  //       content = await page.content();
  //       $ = await cheerio.load(content);
  //       name = $('.ef-drawer').find('h1.u-md-title').text();
  //       const ad1 = $('.ef-exhlist-single__content').find('p').first().contents().eq(0).text();
  //       const ad2 = $('.ef-exhlist-single__content').find('p').first().contents().eq(2).text();
  //       address = `${ad1}, ${ad2}`;
  //       address = address.replace(/\n/g, '').replace(/\t/g, '').replace(/\s\s+/g, ' ');
  //       site = $('.ef-exhlist-single__content').find('p').first().find('a').attr('href');
  //       const q = $('.ef-exhlist-single__section-body', '.ef-drawer').first().children().last();
  //       if (q.get(0).tagName === 'p') {
  //         info = q.text();
  //       }

  //       const s = { name, address, phone, fax, email, site, info };

  //       resolve(s);
  //     }, ms);
  //   });
  // }
  let countItems = $('.card_company').length;

  while (countItems < 721) {
    console.log(countItems);
    await page.evaluate(_ => {
      window.scrollBy(0, window.innerHeight);
      Promise.resolve('1');
    });
    content = await page.content();
    $ = cheerio.load(content);
    countItems = $('.card_company').length;
  }

  let pageLinks = [];
  $('.card_company').each(function (i, el) {
    const link = $(this).attr('href');
    pageLinks.push(link);

  });
  pageLinks = pageLinks.filter(link => link);
  console.log('link count', pageLinks.length);
  page.close();
  // const companies = await page.$$('a.ef-exhlist-stand-item');
  // for (let i = 0; i < companies.length; i++) {
  //   console.log(i);
  //   const element = companies[i];
  //   const o = await timeout(1000, element);
  //   data.push(o);
  //   // console.log(o);
  //   await page.click('.ef-drawer__close');
  //   await page.waitFor(100);
  // }
  const t = await main(pageLinks);
  data = data.concat(t);

  // const pagerCount = $('a', '.pager');

  // for (let i = 1; i < 42; i++) {
  //   pageLinks = [];
  //   await page.click('button.pagingitem.next');
  //   await page.waitForNavigation();
  //   content = await page.content();
  //   $ = cheerio.load(content);
  //   $('li', '#exhibitorsandproductstable').each(function() {
  //     const link = $(this).find('.list-item-content').find('a').first().attr('href');
  //     pageLinks.push(link);
  //   });
  //   pageLinks = pageLinks.filter(link => link);

  //   const w = await main(pageLinks);
  //   data = data.concat(w);
  //   console.log(data.length);
  // }

  console.log('end');
  
  const csv = json2csv({ data, fields });
  const fileName = `pirexpo.csv`;
  fs.writeFile(fileName, csv, (err) => {
    if (err) {
      return console.log(err);
    };
    console.log('file saved');
  });


  browser.close();
}

run();

async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = await getRecordAsync(links[i]);
      promises.push(record);
    }
    const d = await Promise.all(promises);
    return d;

  } catch(err) {
    console.log('error', err);
  }
}

function getRecordAsync(link) {
  console.log(link);
  return new Promise(async (resolve, reject) => {
    const url = `https://pirexpo.com${link}`;
    const page = await browser.newPage();
    await page.goto(url, { timeout: 1000000 });
    let content, $;
    await page.waitForSelector('.grid');
    content = await page.content();
    $ = cheerio.load(content);
  
    let name, address, phone, site, fax, info, email;
  
    name = $('.grid').find('.grid-col-6').find('.title').text();
    info = $('.grid').find('.grid-col-6').find('p').last().text();
    $('.grid').find('.company-info__cell_title').each(function () {
      if ($(this).text().includes('Адрес')) {
        address = $(this).next().text();
      }
      if ($(this).text().includes('Email')) {
        email = $(this).next().text();
      }
      if ($(this).text().includes('Тел')) {
        phone = $(this).next().text();
      }
      if ($(this).text().includes('Сайт')) {
        site = $(this).next().text();
      }
    });
  
    const s = { name, address, phone, fax, email, site, info };
    // console.log(s);
    page.close();
    resolve(s);
  })

}

function getRecord(link) {
  const options = {
    uri: `https://pirexpo.com${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let name, address, phone, site, fax, info, email;
      // name = $('.jl_dt_exhead').find('a#exdetails').text();
      // const a = $('.jl_dt_address', '.jl_dt_exkontakt');
      // const street = a.find('.jl_adr_strasse').text().trim();
      // const city = a.find('.jl_adr_ort').text().trim();
      // const country = a.find('.jl_adr_land').text().trim();
      // address = `${street}, ${city}, ${country}`;
      // phone = $('.jl_dt_kommunikation').find('.adr_phone').text().replace('P', '');
      // fax = $('.jl_dt_kommunikation').find('.adr_fax').text().replace('F', '');

      // email = $('.jl_dt_kommunikation').find('.view_kontakt_mail').find('a').text().trim();
      // site = $('.jl_dt_kommunikation').find('.view_kontakt_url').find('a').attr('href');

      // const div = $('.columns.floatl.clearfix', '.tabbed-content');
      // name = div.find('h3').first().find('a').text().replace(/\n/g, '').replace(/\t/g, '').trim();
      // address = div.find('.adress').text().trim().replace(/\n/g, '').replace(/\t/g, '');
      // site = div.find('.webadress').text().replace('Internet:', '').trim();
      // div.find('div').first().contents().each(function () {
      //   if ($(this).text().includes('Tel.:')) {
      //     phone = $(this).text().replace('Tel.: ', '').trim();
      //   }
      //   if ($(this).text().includes('Fax:')) {
      //     fax = $(this).text().replace('Fax: ', '').trim();
      //   }
      //   if ($(this).text().includes('Email:')) {
      //     email = $(this).text().replace('Email:', '').trim();
      //   }
      // });

      // name = $('.col-lg-6').find('h1').first().text().trim();
      // const block = $('.col-md-6.col-sm-12').find('.col-xs-12').first();
      // address = block.find('.row').eq(1).find('p').text().trim().replace(/\s\s+/g, ' ');;
      // // address = address.replace(/\n/g, ', ');
      // phone = block.find('.row').eq(2).find('p').text().trim();
      // fax = block.find('.row').eq(3).find('p').text().trim();
      // block.find('.row').each(function() {
      //   const a = $(this).find('a');
      //   if (a.length) {
      //     if (a.attr('href').includes('http')) {
      //       site = a.attr('href');
      //     } else {
      //       email = a.text().trim();
      //     }
      //   }
      // });

      name = $('.grid').find('.title').text();
      info = $('.grid').find('.grid-col-6').find('p').last().text();
      $('.grid').find('.company-info__cell_title').each(function() {
        if ($(this).text.includes('Адрес')) {
          address = $(this).next().text();
        }
        if ($(this).text.includes('Email')) {
          email = $(this).next().text();
        }
        if ($(this).text.includes('Тел')) {
          phone = $(this).next().text();
        }
        if ($(this).text.includes('Сайт')) {
          site = $(this).next().text();
        }
      });

      const s = { name, address, phone, fax, email, site, info };
      console.log(s);
      return { name, address, phone, fax, email, site, info };
    })
    .catch(error => {
      console.log('error', error);
    });
}

