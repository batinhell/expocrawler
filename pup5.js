// Всё асинхронно, даже небо, даже аллах
const fs = require('fs');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const puppeteer = require('puppeteer');
const request = require('request-promise');

async function run() {
  let data = [];
  const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'];
  const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
  const page = await browser.newPage();
  const link = 'https://batimat-rus.com/spisok-uchastnikov-vystavki.html';
  await page.goto(link);
  
  let content, $, $$;
  
  function timeout(ms, el) {
    return new Promise(async (resolve, reject) => {
      setTimeout(async function () {
        try {
          console.log('1');
          let name, address, phone, site, fax, info, email, selector;
          // await el.click();
          console.log('2');
          // await page.waitForSelector('.companyinfocontainer', { visible: true });
          // await page.waitFor(200);
          content = await page.content();
          $ = await cheerio.load(content);
          name = $('td.name > h1').text();
          name = name.replace('\n', '').trim();
          info = $('.tab.companyinfocontainer .text').text();
          info = info.replace('ОПИСАНИЕ', '').replace('КАТЕГОРИИ КОМПАНИИ', '').replace(/\n/g, '').replace(/\s+/g, ' ').trim();
          const a = $('.tab.companyinfocontainer .contacts').find('a');
          if (a) {
            site = a.attr('href');
          }
  
          $('.tab.companyinfocontainer .contacts').find('td').first().contents().each(function(index, el) {
            const t = $(this).text();
            if (t.includes('телефон')) {
              phone = t.replace('телефон:', '').trim();
            }
            if (t.includes('факс')) {
              fax = t.replace('факс:', '').trim();
            }
            if (t.includes('Эл. Почта')) {
              email = t.replace('Эл. Почта:', '').trim();
            }
            if ($(this).get(0).type === 'comment') {
              address = $(this).get(0).data.replace(/<br>/g, '').replace(/\n/g, '').replace(/\s+/g, ' ').trim();
            }
          });
  
          const s = { name, address, phone, fax, email, site, info };
  
          resolve(s);
          
        } catch (error) {
          console.log('error', error);
        }
      }, ms);
    });
  }

  await page.waitForSelector('.companylent');
  // content = await page.content();
  // $ = await cheerio.load(content);


  function getPageData(companies) {
    return new Promise(async (resolve, reject) => {
      try {
        for (let i = 0; i < companies.length; i++) {
          await page.waitFor(500);
          // await page.waitForSelector('.companies', { visible: true });
          const element = companies[i];
          await element.click();
          await page.waitFor(500);
          // await page.waitForSelector('.list .article .body', { visible: true });
          const more = await element.$('a.about');
          await page.waitFor(500);
          if (more) {
            await more.click();
          } else {
            continue;
          }
          // await page.waitForSelector('.companyinfocontainer', { visible: true });
          await page.waitFor(500);
          const o = await timeout(600, more);
          const jsonData = JSON.stringify(o);
          fs.appendFileSync('batimat.json', `,${jsonData}`);
          data.push(o);
          await page.click('.head > a.back');
          await page.waitFor(700);
          // await page.waitForSelector('.companies', { visible: true });
        }
        resolve();
      } catch (error) {
        console.log('error2', error);
      }
    })
  }

  // let companies = await page.$$('.companylent > .company');
  // await getPageData(companies);
  let companies;

  for (let i = 0; i < 23; i++) {
    console.log('page - ', i);
    const nextPage = await page.$('.nexponav > a.page.active + a');
    await nextPage.click();
    await page.waitFor(500);
    if (i > -1) {
      companies = await page.$$('.companylent > .company');
      await getPageData(companies);
    }
  }

  console.log(data.length);
  console.log('end');

  const csv = json2csv({ data, fields });
  const fileName = `Batimat-rus.csv`;
  fs.writeFile(fileName, csv, (err) => {
    if (err) {
      return console.log(err);
    };
    console.log('file saved');
  });

  // browser.close();
}

run();


