  function getRecord(link) {
    const options = {
      uri: `http://www.vcudmurtia.ru${link.href}`,
      encoding: 'binary',
      transform(body) {
        const b = new Buffer(body, 'binary');
        const result = iconv.encode(iconv.decode(b, 'win1251'), 'utf8');
        return cheerio.load(result);
      },
    };

    return request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email;
        name = link.name;
        if (!name) {
          name =  $('table').find('td').eq(0).find('b').first().text();
        }
        info = $('table').find('td').eq(0).find('p').last().text();

        $('table').find('td').eq(0).find('a').each(function () {
          const href = $(this).attr('href');
          if (href.includes('mailto')) {
            email = $(this).text();
          }

          if (href.includes('http')) {
            site = href;
          }
        });

        $('table').find('td').eq(0).contents().each(function () {
          address = $('table').find('td').eq(0).contents().eq(2).prevObject['5'].data;
          if ($(this).text().includes('тел.')) {
            phone = $(this).text().trim().replace('тел. ', '');
          }
          if ($(this).text().includes('факс')) {
            fax = $(this).text().trim().replace('факс: ', '');
          }
        });

        const s = { name, address, phone, fax, email, site, info };
        // console.log(s);
        return { name, address, phone, fax, email, site, info };
      })
      .catch(error => {
        console.log('error', error);
      });
  }


  name = $('h1[itemprop = "name"]').first().text();
  fax = $('div[itemprop = "fax"]').first().text().replace('Fax: ', '');
  phone = $('div[itemprop = "phone"]').first().find('a').text().trim();
  site = $('a[itemprop = "url"]', '.contact-company-block').first().attr('href');
  const street = $('div[itemprop = "street"]').first().text().trim();
  const city = $('div[itemprop = "city"]').first().text().trim();
  const country = $('div[itemprop = "country"]').first().text().trim();
  address = `${country}, ${city}, ${street}`;
  info = $('p.intro').text();


const fs = require('fs');
const request = require('request-promise');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const iconv = require('iconv-lite');

const config = {
  name: 'DIAM',
  site: 'http://bochum.diam.de/bochum/aussteller/ausstellerverzeichnis.html',
  fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
  linkSelector: 'a',
};

const options = {
  uri: config.site,
  // encoding: 'binary',
  transform(body) {
    // const b = new Buffer(body, 'binary');
    // const result = iconv.encode(iconv.decode(b, 'win1251'), 'utf8');
    return cheerio.load(body);
  }
};

request(options)
  .then($ => {
    let participantLinks = [];
    p = $('.col-sm-12', '.row').find('a').filter(function (i, el) {
      return !$(this).attr('href').includes('letter') &&
             !$(this).attr('href').includes('#') &&
             !$(this).attr('href').includes('xoffset') &&
             $(this).attr('href').includes('http');
    });
    p.each(function () {
      const link = $(this).attr('href');
      participantLinks.push(link);
    });
    participantLinks = participantLinks.filter((el, i, a) => i === a.indexOf(el))
    console.log(participantLinks.length);
    main(participantLinks);
  })
  .catch(error => {
    console.log(error);
  });

  async function main(links) {
    try {
      let promises = [];
      for (let i = 0; i < links.length; i++) {
        const record = getRecord(links[i]);
        promises.push(record);
      }
      const data = await Promise.all(promises);
      const csv = json2csv({ data: data, fields: config.fields });
      const fileName = `${config.name}.csv`;
      fs.writeFile(fileName, csv, (err) => {
        if (err) {
          return console.log(err);
        };
        console.log('file saved');
      });

    } catch(err) {
      console.log('error', err);
    }
  }

  function getRecord(link) {
    const options = {
      uri: `${link}`,
      transform(body) {
        return cheerio.load(body);
      },
    };

    return request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email;
        name = $('.content_firma').find('h2').first().text();
        address = $('.content_anschrift').find('p').first().text().trim().replace(/\n/g, ',');
        info = $('.col-sm-12', '.tabcontent_zusatztext').find('p').first().text().trim();
        $('td', '.content_kontakt').each(function() {
          if ($(this).text() === 'Tel:') {
            phone = $(this).next().next().text().trim();
          }

          if ($(this).text() === 'E-Mail:') {
            email = $(this).next().next().text().trim();
          }

          if ($(this).text() === 'Fax:') {
            fax = $(this).next().next().text().trim();
          }
          if ($(this).text() === 'Webseite:') {
            site = $(this).next().next().text().trim();
          }
        });
        // const s = { name, address, phone, fax, email, site, info };
        // console.log(s);
        return { name, address, phone, fax, email, site, info };
      })
      .catch(error => {
        console.log('error', error);
      });
  }


  name = $('.firmenadresse').find('strong').first().text();
  const ad1 = $('.firmenadresse').contents().eq(4).text().trim();
  const ad2 = $('.firmenadresse').contents().eq(6).text().trim();
  address = `${ad1} ${ad2}`;
  info = $('.beschreibung').text().trim();
  $('.firmenadresse').contents().each(function () {
    const t = $(this).text().trim();
    // console.log(t);
    if (t.includes('www')) {
      site = t;
    }

    if (t.includes('@')) {
      email = t;
    }

    if (t.includes('Tel.')) {
      phone = t.replace('Tel.', '');
    }

    if (t.includes('Fax.')) {
      fax = t.replace('Fax.', '');
    }
  });



  const fs = require('fs');
  const request = require('request-promise');
  const cheerio = require('cheerio');
  const json2csv = require('json2csv');
  
  const config = {
    name: 'Промэкспо',
    site: 'http://volgogradexpo.ru/companies/',
    fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
    linkSelector: 'a',
  };
  
  let data = [];
  
  const options = {
    uri: config.site,
    transform(body) {
      return cheerio.load(body);
    }
  };
  
  request(options)
    .then($ => {
      let pageLinks = [];
      $('li', '.sort-results').each(function() {
        const link = $(this).find('a').attr('href');
        pageLinks.push(link);
      });
      pageLinks = pageLinks.filter((el, i, a) => i === a.indexOf(el));
      mainPage(pageLinks);
      
    })
    .catch(error => {
      console.log(error);
    });
  
  async function main(links) {
    try {
      let promises = [];
      for (let i = 0; i < links.length; i++) {
        const record = getRecord(links[i]);
        // promises.push(record);
      }
      // await Promise.all(promises);
  
    } catch(err) {
      console.log('error', err);
    }
  }
  
  async function mainPage(links) {
    try {
      let promises = [];
      for (let i = 0; i < links.length; i++) {
        const record = await getPage(links[i]);
        promises.push(record);
      }
      await Promise.all(promises);
  
      const csv = json2csv({ data: data, fields: config.fields });
      const fileName = `${config.name}.csv`;
      fs.writeFile(fileName, csv, (err) => {
        if (err) {
          return console.log(err);
        };
        console.log('file saved');
      });
  
    } catch(err) {
      console.log('error', err);
    }
  }
  
  function getPage(link) {
    console.log(link);
    const options = {
      uri: `http://www.hannovermesse.de${link}`,
      transform(body) {
        return cheerio.load(body);
      },
    };
  
    return request(options)
      .then($ => {
        let companyLinks = [];
        $('li', '.exhibitor-list').each(function() {
          const link = $(this).find('a').attr('href');
          companyLinks.push(link);
        });
    
        main(companyLinks);
      })
      .catch(error => {
        console.log('error', error);
      });
  }
  
  function getRecord(link) {
    const options = {
      uri: `http://www.hannovermesse.de${link}`,
      transform(body) {
        return cheerio.load(body);
      },
    };
  
    request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email;
        name = $('h1[itemprop = "name"]').first().text();
        fax = $('div[itemprop = "fax"]').first().text().replace('Fax: ', '');
        phone = $('div[itemprop = "phone"]').first().find('a').text().trim();
        site = $('a[itemprop = "url"]', '.contact-company-block').first().attr('href');
        const street = $('div[itemprop = "street"]').first().text().trim();
        const city = $('div[itemprop = "city"]').first().text().trim();
        const country = $('div[itemprop = "country"]').first().text().trim();
        address = `${country}, ${city}, ${street}`;
        info = $('p.intro').text();
        const o = { name, address, phone, fax, site, email, info };
        data.push(o);
        console.log(data.length);
      })
      .catch(error => {
        console.log('error', error);
      });
  }
  
  const fs = require('fs');
  const cheerio = require('cheerio');
  const json2csv = require('json2csv');
  const puppeteer = require('puppeteer');
  const request = require('request-promise');
  
  
  async function run() {
    let data = [];
    const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info', 'brand'];
    const browser = await puppeteer.launch({args: ['--no-sandbox']});
    const page = await browser.newPage();
    const link = 'http://www.pcvexpo.ru/ru-RU/about/exhibitor-list.aspx';
    await page.goto(link);
  
    let content, $;
    content = await page.content();
    $ = cheerio.load(content);
  
  
    let pageLinks = [];
    $('em', '.exhibitorlist').each(function() {
      const link = $(this).find('a.name').first().attr('href');
      pageLinks.push(link);
    });
    pageLinks = pageLinks.filter(link => link);
    
    const t = await main(pageLinks);
    data = data.concat(t);
  
    const pagerCount = $('a', '.pager');
  
    for (let i = 0; i < pagerCount.length; i++) {
      pageLinks = [];
      await page.click('.pager > span + a');
      await page.waitForNavigation();
      content = await page.content();
      $ = cheerio.load(content);
      $('em', '.exhibitorlist').each(function() {
        const link = $(this).find('a.name').first().attr('href');
        pageLinks.push(link);
      });
      pageLinks = pageLinks.filter(link => link);
  
      const w = await main(pageLinks);
      data = data.concat(w);
      console.log(data.length);
    }
  
    console.log('end');
    
    const csv = json2csv({ data, fields });
    const fileName = `Pcvexpo.csv`;
    fs.writeFile(fileName, csv, (err) => {
      if (err) {
        return console.log(err);
      };
      console.log('file saved');
    });
  
  
    browser.close();
  }
  
  run();
  
  async function main(links) {
    try {
      let promises = [];
      for (let i = 0; i < links.length; i++) {
        const record = await getRecord(links[i]);
        promises.push(record);
      }
      const d = await Promise.all(promises);
      return d;
  
    } catch(err) {
      console.log('error', err);
    }
  }
  
  function getRecord(link) {
    const options = {
      uri: `http://www.pcvexpo.ru${link}`,
      transform(body) {
        return cheerio.load(body);
      },
    };
  
    return request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email, brand;
        // name = $('h2', '.scorecard').first().text().trim();
        // address = $('div', '.scorecard').eq(1).find('p').text().trim();
        // phone = $('div', '.scorecard').eq(3).find('div').find('p').text().trim();
        // fax = $('div', '.scorecard').eq(5).find('div').find('p').text().trim();
        // site = $('div', '.scorecard').eq(7).find('div').find('a').attr('href');
        // email = $('div', '.scorecard').eq(9).find('div').find('a').text().trim();
        // info = $('div', '.scorecard').eq(11).find('div').find('p').text().trim();
        // const brandEl = $('div', '.scorecard').eq(13).find('p').get(0);
        // brand = brandEl ? brandEl.next.data : '';
        name = $('.admin_add').find('h2').first().text().trim();
        // $('div', '.admin_add').each(function () {
        //   const strong = $(this).find('strong');
        //   if (strong.length && strong.text() === 'Адрес') {
        //     address = $(this).contents().eq(2).text().trim();
        //   }
        //   if (strong.length && strong.text() === 'Номера телефона') {
        //     phone = $(this).contents().eq(2).text().trim();
        //   }
        //   if (strong.length && strong.text() === 'Номер факса') {
        //     fax = $(this).contents().eq(2).text().trim();
        //   }
        //   if (strong.length && strong.text() === 'Сайт') {
        //     site = $(this).contents().eq(3).attr('href');
        //   }
        //   if (strong.length && strong.text() === 'Описание компании') {
        //     info = $(this).contents().eq(2).text().trim();
        //   }
        //   if (strong.length && strong.text() === 'Бренды') {
        //     brand = $(this).contents().eq(1).text().trim();
        //   }
        // });
        // console.log(address);
        $('div.name', '.exhibitorview').each(function () {
          if ($(this).find('strong').text() === 'Адрес') {
            address = $(this).next().text().trim();
          }
          if ($(this).find('strong').text() === 'Номера телефона') {
            phone = $(this).next().text().trim();
          }
          if ($(this).find('strong').text() === 'Номер факса') {
            fax = $(this).next().text().trim();
          }
          if ($(this).find('strong').text() === 'Сайт') {
            site = $(this).next().find('a').attr('href');
          }
          if ($(this).find('strong').text() === 'Email') {
            email = $(this).next().find('a').text();
          }
          if ($(this).find('strong').text() === 'Описание компании') {
            info = $(this).next().text().trim();
          }
          if ($(this).find('strong').text() === 'Бренды') {
            brand = $(this).get(0).next.data.trim();
          }
        });
  
  
  
        const s = { name, address, phone, fax, email, site, info, brand };
        // console.log(s);
        return { name, address, phone, fax, email, site, info, brand };
      })
      .catch(error => {
        console.log('error', error);
      });
  }
  
  let address, phone, site, fax, info, email;
  $('.scorecard').find('div').each(function (i, el) {
    if ($(this).find('strong').text() === 'Страна RU:') {
      const t = $(this).text().replace('Страна RU:', '').trim();
      address.push(t);
    }

    if ($(this).find('strong').text() === 'Город RU:') {
      const t = $(this).text().replace('Город RU:', '').trim();
      address.push(t);
    }

    if ($(this).find('strong').text() === 'Адрес:') {
      const t = $(this).text().replace('Адрес:', '').trim();
      address.push(t);
    }

    if ($(this).find('strong').text() === 'Телефон:') {
      const t = $(this).text().replace('Телефон:', '').trim();
      phone = t;
    }

    if ($(this).find('strong').text() === 'Факс:') {
      const t = $(this).text().replace('Факс:', '').trim();
      fax = t;
    }

    if ($(this).find('strong').text() === 'E-mail:') {
      const t = $(this).text().replace('E-mail:', '').trim();
      email = t;
    }

    if ($(this).children().first().length && $(this).children().first().get(0).tagName === 'a') {
      site = $(this).find('a').attr('href');
    }

    if (!$(this).find('strong').length && !$(this).find('a').length) {
      info = $(this).text()
    }

    // if ($(this).get(0).tagName === 'dt' && $(this).text() === 'Адрес: ') {
    //   address = $(this).next().text();
    // }

  });

  address = address.join(', ');


  name = $('.ngn-page-title').find('bdi').text().trim();
  $('.ngn-address').find('span').each(function() {
    address.push($(this).text().trim());
  });
  address = address.join(', ');
  phone = $('.ngn-table').find('bdi[itemprop="telephone"]').text().trim();
  fax = $('.ngn-table').find('bdi[itemprop="faxNumber"]').text().trim();
  site = $('.ngn-website').find('a').attr('href');
  info = $('.ngn-description').find('bdi[itemprop="description"]').text().trim();


  name = $('.tx-dgm-exhibition').find('h1').first().text().trim();
  const last = $('.col-md-8', '.tx-dgm-exhibition').children().last();
  site = last.find('i.fa-globe').next().attr('href');
  email = last.find('i.fa-envelope').next().text().trim();
  phone = last.find('i.fa-phone').get(0) && last.find('i.fa-phone').get(0).next.data.trim();
  fax = last.find('i.fa-fax').get(0) && last.find('i.fa-fax').get(0).next.data.trim();


  name = $('.cols.cols-50').find('h2').text().trim();
  const block = $('#zeile_aussteller_infoblock');
  address = block.find('address').text().trim().replace(/\n/g, '');
  phone = block.find('i.fa-phone').get(0) && block.find('i.fa-phone').get(0).next.data.trim();
  fax = block.find('i.fa-fax').get(0) && block.find('i.fa-fax').get(0).next.data.trim();
  email = block.find('a[title="E-mail"]').attr('href') && block.find('a[title="E-mail"]').attr('href').replace('mailto:', '');
  site = block.find('a[title="Homepage"]').attr('href');
  info = $('.aussteller_info_block').find('.dynamic-wrap').find('p').first().text().trim();




  function getRecord(link) {
    const options = {
      uri: `${link}`,
      transform(body) {
        return cheerio.load(body);
      },
    };
  
    return request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email, selector;
        name = $('.trail').next().text();
        const container = $('#data_container').find('p');
        if (!container.length) {
          $('#data_container').contents().each(function() {
            const t = $(this).text().trim();
            if (t.includes('www')) {
              site = t;
            }
            if (t.includes('@')) {
              email = t;
            }
            if (t.includes('Тел')) {
              if (t.includes('Телефон')) {
                phone = t.trim().replace('Телефон: ', '');
              } else if(t.includes('Тел./факс')) {
                phone = t.trim().replace('Тел./факс: ', '');
              } else if(t.includes('Тел.:')) {
                phone = t.trim().replace('Тел.:', '');
              } else {
                phone = t.trim().replace('Тел.: ', '');
              }
            }
            if (t.includes('Факс:')) {
              fax = t.trim().replace('Факс: ', '');
            }
            if (t.includes('г.')) {
              address = $(this).text().trim();
            }
          });
        }
        const hasA = $('#data_container').find('p').first().find('a').length;
        if (hasA) {
          if ($('#data_container').find('p').first().find('font').length) {
            selector = $('#data_container').find('p').first().find('font').first();
            info = $('#data_container').find('p').eq(1).find('font').first().text().trim();
          } else {
            selector = $('#data_container').find('p').eq(0);
            info = $('#data_container').find('p').eq(1).text().trim();
          }
        } else {
          selector = $('#data_container').find('p').eq(1);
          info = $('#data_container').find('p').eq(2).text().trim();
        }
        selector.contents().each(function() {
          const t = $(this).text().trim();
          if (t.includes('www')) {
            site = t;
          }
          if (t.includes('@')) {
            email = t;
          }
          if (t.includes('Тел')) {
            if (t.includes('Телефон')) {
              phone = t.trim().replace('Телефон: ', '');
            } else if(t.includes('Тел./факс')) {
              phone = t.trim().replace('Тел./факс: ', '');
            } else if(t.includes('Тел.:')) {
              phone = t.trim().replace('Тел.:', '');
            } else {
              phone = t.trim().replace('Тел.: ', '');
            }
          }
          if (t.includes('Факс:')) {
            fax = t.trim().replace('Факс: ', '');
          }
          if (t.includes('г.')) {
            address = $(this).text().trim();
          }
        });
        const s = { name, address, phone, fax, email, site, info }
        // console.log(s);
        return { name, address, phone, fax, email, site, info };
      })
      .catch(error => {
        console.log('error', error);
      });
  }



  name = $('.exhibitor-name').find('strong').text().trim();
  const ad1 = $('.exhibitor-address').contents().eq(0).text().trim();
  const ad2 = $('.exhibitor-address').contents().eq(2).text().trim();
  const ad3 = $('.exhibitor-address').contents().eq(4).text().trim();
  address = `${ad1}, ${ad2}, ${ad3}`;
  $('.exhibitor-address').contents().each(function() {
    if ($(this).text().includes('Tel.')) {
      phone = $(this).text().replace('Tel.: ', '').trim();
    }
    if ($(this).text().includes('Fax:')) {
      fax = $(this).text().replace('Fax: ', '').trim();
    }
  });
  $('.exhibitor-address').find('a').each(function() {
    if ($(this).attr('href').includes('http')) {
      site = $(this).attr('href');
    }
    if ($(this).attr('href').includes('@')) {
      email = $(this).text();
    }
  });

  const div = $('.columns.floatl.clearfix', '.tabbed-content');
  name = div.find('h3').first().find('a').text().replace(/\n/g, '').replace(/\t/g, '').trim();
  address = div.find('.adress').text().trim().replace(/\n/g, '').replace(/\t/g, '');
  site = div.find('.webadress').text().replace('Internet:', '').trim();
  div.find('div').first().contents().each(function () {
    if ($(this).text().includes('Tel.:')) {
      phone = $(this).text().replace('Tel.: ', '').trim();
    }
    if ($(this).text().includes('Fax:')) {
      fax = $(this).text().replace('Fax: ', '').trim();
    }
    if ($(this).text().includes('Email:')) {
      email = $(this).text().replace('Email:', '').trim();
    }
  });

  name = $('.col-lg-6').find('h1').first().text().trim();
  const block = $('.col-md-6.col-sm-12').find('.col-xs-12').first();
  address = block.find('.row').eq(1).find('p').text().trim().replace(/\t/g, '');
  address = address.replace(/\n/g, ', ');
  phone = block.find('.row').eq(2).find('p').text().trim();
  fax = block.find('.row').eq(3).find('p').text().trim();
  block.find('.row').each(function() {
    const a = $(this).find('a');
    if (a.length) {
      if (a.attr('href').includes('http')) {
        site = a.attr('href');
      } else {
        email = a.text().trim();
      }
    }
  });

  const div = $('.blocco-bianco.espositore');
  name = div.find('h3').first().text().trim();
  address = div.find('i.fa-map-marker').get(0).next.data.replace(/\n/g, '').replace(/\t/g, '').trim();
  phone = div.find('.pull-left.mr-20').find('i.fa-phone').get(0).next.data.replace(/\n/g, '').replace(/\t/g, '').trim();
  fax = div.find('.pull-left.mr-20').find('i.fa-fax').get(0).next.data.replace(/\n/g, '').replace(/\t/g, '').trim();
  email = div.find('i.fa-envelope').next().text().trim();
  site = div.find('i.fa-external-link').next().attr('href').trim();



  let pageLinks = [config.site];
  $('li', '.f3-widget-paginator').not('.next').not('.current').each(function() {
    const link = $(this).find('a').attr('href');
    pageLinks.push(link);
  });
  pageLinks = pageLinks.filter((el, i, a) => i === a.indexOf(el));
  
  $('.element-spacing-small-bottom').each(function () {
    let name, address, phone, site, fax, info, email;
    const ad1 = $('address', this).contents().eq(3).text().trim();
    const ad2 = $('address', this).contents().eq(5).text().trim();
    const ad3 = $('address', this).contents().eq(7).text().trim();
    address = `${ad1} ${ad2} ${ad3}`;
    name = $('strong', this).find('a').text();
    site = $('strong', this).find('a').attr('href');
    if ($('abbr[title = "Telefon"]', this).length) {
      phone = $('abbr[title = "Telefon"]', this).get(0).next.data.trim();
    }
    if ($('abbr[title = "Fax"]', this).length) {
      fax = $('abbr[title = "Fax"]', this).get(0).next.data.trim();
    }
    email = $('abbr[title = "Email"]', this).next().text();
    const o = { name, address, phone, fax, site, email, info };
    data.push(o);
  });
  

  name = $('.vcard').find('p.mb0').find('strong').text().trim();
  ad1 = $('.vcard').find('p.mt0').find('span.street-address').text().trim();
  ad2 = $('.vcard').find('p.mt0').find('span.postal-code').text().trim();
  ad3 = $('.vcard').find('p.mt0').find('span.region').text().trim();
  ad4 = $('.vcard').find('p.mt0').find('span.country-name').text().trim();
  address = `${ad1}, ${ad2}, ${ad3}, ${ad4}`;
  $('.vcard').find('span.tel').each(function () {
    const s = $(this).find('span.type');
    if (s.text() === 'Telefon:') {
      phone = s.get(0).next.data.trim();
    }
    if (s.text() === 'Fax:') {
      fax = s.get(0).next.data.trim();
    }
  });

  email = $('.vcard').find('span.email').find('a').text();
  site = $('.vcard').find('span.www').find('a').attr('href');



  let address = [], phone, site, fax, info, email;
  const div = $('.contact-company-block', '.flex-grid');
  name = div.find('h3[itemprop="companyName"]').text().replace(/\s\s+/g, ' ');
  const ad1 = div.find('div[itemprop="street"]').text();
  const ad2 = div.find('div[itemprop="city"]').text();
  const ad3 = div.find('div[itemprop="country"]').text();
  address = `${ad1}, ${ad2}, ${ad3}`;
  address = address.replace(/\s\s+/g, ' ');
  phone = div.find('div[itemprop="phone"]').find('a.tel-number').text().replace(/\s\s+/g, ' ');
  fax = div.find('div[itemprop="fax"]').text().replace('Fax:', '');
  site = div.find('a[itemprop="url"]').attr('href');