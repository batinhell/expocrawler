// GKOP
const content = $('td.content').find('table').find('tr').first().find('td').eq(1);
info = $('td.content').find('table').find('tr').eq(2).find('td').first().text();
address = content.find('p').eq(1).text().trim();
content.find('p').each(function () {
  console.log($(this).text());
  if ($(this).text().includes('Тел')) {
    phone = $(this).text().replace('Тел. ', '').trim();
  }
  if ($(this).text().includes('E-mail')) {
    email = $(this).text().replace('E-mail: ', '').trim();
  }
  if ($(this).text().includes('www')) {
    site = $(this).text().trim();
  }
});
name = $('td.content').find('h1').first().text();

// pirexpo
name = $('.grid').find('.grid-col-6').find('.title').text();
info = $('.grid').find('.grid-col-6').find('p').last().text();
$('.grid').find('.company-info__cell_title').each(function () {
  if ($(this).text().includes('Адрес')) {
    address = $(this).next().text();
  }
  if ($(this).text().includes('Email')) {
    email = $(this).next().text();
  }
  if ($(this).text().includes('Тел')) {
    phone = $(this).next().text();
  }
  if ($(this).text().includes('Сайт')) {
    site = $(this).next().text();
  }
});

// batimat
name = $('td.name > h1').text();
name = name.replace('\n', '').trim();
info = $('.tab.companyinfocontainer .text').text();
info = info.replace('ОПИСАНИЕ', '').replace('КАТЕГОРИИ КОМПАНИИ', '').replace(/\n/g, '').replace(/\s+/g, ' ').trim();
const a = $('.tab.companyinfocontainer .contacts').find('a');
if (a) {
  site = a.attr('href');
}

$('.tab.companyinfocontainer .contacts').find('td').first().contents().each(function (index, el) {
  const t = $(this).text();
  if (t.includes('телефон')) {
    phone = t.replace('телефон:', '').trim();
  }
  if (t.includes('факс')) {
    fax = t.replace('факс:', '').trim();
  }
  if (t.includes('Эл. Почта')) {
    email = t.replace('Эл. Почта:', '').trim();
  }
  if ($(this).get(0).type === 'comment') {
    address = $(this).get(0).data.replace(/<br>/g, '').replace(/\n/g, '').replace(/\s+/g, ' ').trim();
  }
});

// worldbuild-krasnodar
name = $('.admin_add').find('h2').first().text().trim();
$('div.name', '.exhibitorview').each(function () {
  if ($(this).find('strong').text() === 'Адрес') {
    address = $(this).next().text().trim();
  }
  if ($(this).find('strong').text() === 'Номера телефона') {
    phone = $(this).next().text().trim();
  }
  if ($(this).find('strong').text() === 'Номер факса') {
    fax = $(this).next().text().trim();
  }
  if ($(this).find('strong').text() === 'Сайт') {
    site = $(this).next().find('a').attr('href');
  }
  if ($(this).find('strong').text() === 'Email') {
    email = $(this).next().find('a').text();
  }
  if ($(this).find('strong').text() === 'Описание компании') {
    info = $(this).next().text().trim();
  }
  if ($(this).find('strong').text() === 'Бренды') {
    brand = $(this).get(0).next.data.trim();
  }
});

// online.messefrankfurt
name = $('.text').find('h1').text();
info = $('.text').find('p').text().replace(/\n/g, '').replace(/\t+/g, ' ').trim();
const ad1 = $('.contacts').find('ul').children().eq(0).text();
const ad2 = $('.contacts').find('ul').children().eq(1).text();
address = `${ad1}, ${ad2}`;

$('.contacts').find('ul').find('li').each(function () {
  if ($(this).text().includes('Телефон')) {
    phone = $(this).text().replace('Телефон: ', '').trim();
    console.log($(this).text());
  }
  if ($(this).text().includes('Факс')) {
    fax = $(this).text().replace('Факс: ', '').trim();
  }
  if ($(this).text().includes('Email')) {
    email = $(this).text().replace('Email: ', '').trim();
  }
});

site = $('.www').find('a').attr('href');

// beviale-moscow
const div = $('#maintop');
name = div.find('span[itemprop="name"]').text();
const ad1 = div.find('span[itemprop="addressLocality"]').text();
const ad2 = div.find('span[itemprop="postalCode"]').text();
const ad3 = div.find('span[itemprop="addressCountry"]').text();
const ad4 = div.find('span[itemprop="streetAddress"]').text();
address = `${ad3}, ${ad2}, ${ad1}, ${ad4}`;
address = address.replace(/\s\s+/g, ' ');
phone = div.find('span[itemprop="telephone"]').text();
fax = div.find('span[itemprop="faxNumber"]').text();
site = div.find('p.mt10').find('a.bold').attr('href');
info = div.find('.rWrapper').last().find('p').text();

// ipls-russia
name = $('h2.exhibitorName').text().trim();
site = $('a.webAddress').attr('href');
info = $('p.description').text().replace(/\n/g, '').replace(/\s+/g, ' ').trim();
const ad1 = $('span.extended-address', '.adr').text();
const ad2 = $('span.locality', '.adr').text();
const ad3 = $('span.postal-code', '.adr').text();
const ad4 = $('span.country-name', '.adr').text();
address = `${ad4}, ${ad3}, ${ad2}, ${ad1}`;
$('span', 'div.tel').each(function () {
  if ($(this).text() === 'Тел.:') {
    phone = $(this).next().text();
  }
  if ($(this).text() === 'Факс:') {
    fax = $(this).next().text();
  }
});

// OSMO
name = $('#content').find('h1').first().contents().eq(0).text();
info = $('#content').find('div.gsePartnerPageSlogan').text();

const p = $('#content').find('p.gseHeader').last();

p.prev().contents().each(function () {
  const t = $(this).text();
  if (t === 'Адрес') {
    address = $(this).get(0).next.data.replace(': ', '').trim();
  }
  if (t === 'Телефон') {
    phone = $(this).get(0).next.data.replace(': ', '').trim();
  }
  if (t === 'Факс') {
    fax = $(this).get(0).next.data.replace(': ', '').trim();
  }
});

site = $('.gseLinkList').find('a').attr('href');
email = p.prev().find('a').text();