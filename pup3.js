const fs = require('fs');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const puppeteer = require('puppeteer');
const request = require('request-promise');


function timeout(ms, el) {
  return new Promise((resolve, reject) => {
    setTimeout(async function() {
      await el.click();
      await page.waitForSelector('.popupContent');
      content = await page.content();
      $ = await cheerio.load(content);
      const name = $('.popupContent').find('div[itemprop="legalName"]').text();
      resolve(name);
    }, ms);
  });
}

async function run() {
  let data = [];
  const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info', 'brand'];
  const browser = await puppeteer.launch({headless: true, args: ['--no-sandbox']});
  const page = await browser.newPage();
  const link = 'http://www.deburring-expo.de/en/visitors/exhibitors-and-products-2017/#/search/c=50';
  await page.goto(link);

  let content, $, $$;

  function timeout(ms, el) {
    return new Promise((resolve, reject) => {
      setTimeout(async function() {
        let name, address, phone, site, fax, info, email, selector;
        await el.click();
        await page.waitFor(500);
        content = await page.content();
        $ = await cheerio.load(content);
        name = $('.popupContent').find('div[itemprop="legalName"]').text();
        const ad1 = $('.popupContent').find('div[itemprop="streetAddress"]').find('div').first().text();
        const ad2 = $('.popupContent').find('div[itemprop="postalCode"]').text();
        const ad3 = $('.popupContent').find('div[itemprop="addressLocality"]').text();
        const ad4 = $('.popupContent').find('div[itemprop="addressCountry"]').text();
        address = `${ad1}, ${ad2} ${ad3}, ${ad4}`;

        phone = $('.popupContent').find('div[itemprop="telephone"]').text();
        fax = $('.popupContent').find('div[itemprop="faxNumber"]').text();
        site = $('.popupContent').find('a[itemprop="url"]').attr('href');
        email = $('.popupContent').find('a[itemprop="email"]').text();

        const s = { name, address, phone, fax, email, site, info };

        resolve(s);
      }, ms);
    });
  }

  await page.waitForSelector('.OB0JQR-l-E');
  content = await page.content();
  $ = await cheerio.load(content);
  
  let countItems = $('.OB0JQR-l-E').length;
  
  while (countItems < 182) {
    await page.click('button.OB0JQR-f-i');
    content = await page.content();
    $ = await cheerio.load(content);
    countItems = $('.OB0JQR-l-E').length;
  }
  
  const companies = await page.$$('.OB0JQR-l-E');

  for (let i = 0; i < companies.length; i++) {
    console.log(i);
    const element = companies[i];
    console.log('1');
    const o = await timeout(1000, element);
    console.log('2');
    data.push(o);
    // console.log(o);
    await page.click('.OB0JQR-c-ec.OB0JQR-b-w');
    await page.waitFor(100);
  }

  // let pageLinks = [];
  // $('em', '.exhibitorlist').each(function() {
  //   const link = $(this).find('a.name').first().attr('href');
  //   pageLinks.push(link);
  // });
  // pageLinks = pageLinks.filter(link => link);
  
  // const t = await main(pageLinks);
  // data = data.concat(t);


  console.log(data.length);
  console.log('end');

  const csv = json2csv({ data, fields });
  const fileName = `DeburringEXPO.csv`;
  fs.writeFile(fileName, csv, (err) => {
    if (err) {
      return console.log(err);
    };
    console.log('file saved');
  });


  browser.close();
}

run();

async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = await getRecord(links[i]);
      promises.push(record);
    }
    const d = await Promise.all(promises);
    return d;

  } catch(err) {
    console.log('error', err);
  }
}

function getRecord(link) {
  const options = {
    uri: `http://www.pcvexpo.ru${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let name, address, phone, site, fax, info, email, brand;
      
      
      const s = { name, address, phone, fax, email, site, info, brand };
      // console.log(s);
      return { name, address, phone, fax, email, site, info, brand };
    })
    .catch(error => {
      console.log('error', error);
    });
}

