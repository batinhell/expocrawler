const fs = require('fs');
const request = require('request-promise');
const cheerio = require('cheerio');
const json2csv = require('json2csv');

const config = {
  name: 'FMBA',
  site: 'http://www.fmb-messe.de/fuer-besucher/ausstellerverzeichnis/exhibitors/a-z/',
  fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
  linkSelector: 'a',
};

let data = [];

const options = {
  uri: config.site,
  transform(body) {
    return cheerio.load(body);
  }
};

request(options)
  .then($ => {
    let alphaLinks = [];
    $('.tx-clarion-events-list-alphabet').find('a').each(function () {
      const link = $(this).attr('href');
      alphaLinks.push(link);
    });

    let pageLinks = [config.site];

    $('li', '.f3-widget-paginator').not('.next').not('.current').each(function() {
      const link = $(this).find('a').attr('href');
      pageLinks.push(link);
    });
    pageLinks = pageLinks.filter((el, i, a) => i === a.indexOf(el));
    
    $('.element-spacing-small-bottom').each(function () {
      let name, address, phone, site, fax, info, email;
      const ad1 = $('address', this).contents().eq(3).text().trim();
      const ad2 = $('address', this).contents().eq(5).text().trim();
      const ad3 = $('address', this).contents().eq(7).text().trim();
      address = `${ad1} ${ad2} ${ad3}`;
      name = $('strong', this).find('a').text();
      site = $('strong', this).find('a').attr('href');
      phone = $('abbr[title = "Telefon"]', this).get(0).next.data.trim();
      if ($('abbr[title = "Fax"]', this).length) {
        fax = $('abbr[title = "Fax"]', this).get(0).next.data.trim();
      }
      email = $('abbr[title = "Email"]', this).next().text();
      const o = { name, address, phone, fax, site, email, info };
      data.push(o);
    });
    main(pageLinks);
    
  })
  .catch(error => {
    console.log(error);
  });

async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = getRecord(links[i]);
      promises.push(record);
    }
    await Promise.all(promises);
    const csv = json2csv({ data: data, fields: config.fields });
    const fileName = `${config.name}.csv`;
    fs.writeFile(fileName, csv, (err) => {
      if (err) {
        return console.log(err);
      };
      console.log('file saved');
    });

  } catch(err) {
    console.log('error', err);
  }
}

function getRecord(link) {
  const options = {
    uri: `http://www.fmb-messe.de/${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      $('.element-spacing-small-bottom').each(function () {
        let name, address, phone, site, fax, info, email;
        const ad1 = $('address', this).contents().eq(3).text().trim();
        const ad2 = $('address', this).contents().eq(5).text().trim();
        const ad3 = $('address', this).contents().eq(7).text().trim();
        address = `${ad1} ${ad2} ${ad3}`;
        name = $('strong', this).find('a').text();
        site = $('strong', this).find('a').attr('href');
        phone = $('abbr[title = "Telefon"]', this).get(0).next.data.trim();
        if ($('abbr[title = "Fax"]', this).length) {
          fax = $('abbr[title = "Fax"]', this).get(0).next.data.trim();
        }
        email = $('abbr[title = "Email"]', this).next().text();
        const o = { name, address, phone, fax, site, email, info };
        data.push(o);
      });
      console.log(data.length);
      // const s = { name, address, phone, fax, email, site, info };
      // console.log(s);
      // return { name, address, phone, fax, email, site, info };
    })
    .catch(error => {
      console.log('error', error);
    });
}

