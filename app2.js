const fs = require('fs');
const request = require('request-promise');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const iconv = require('iconv-lite');

const config = {
  name: 'Инлегмаш',
  site: 'http://catalog.expocentr.ru/catalog.php?wyst_id=102&info_id=0',
  fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
};

const options = {
  uri: config.site,
  encoding: 'binary',
  transform(body) {
    // Бывают сайты с кодировкопроблемами. Такое декодирование обычно помогает
    const b = new Buffer(body, 'binary');
    const result = iconv.encode(iconv.decode(b, 'win1251'), 'utf8');
    return cheerio.load(result);
  }
};

request(options)
  .then($ => {
    let participantLinks = [];
    $('a', 'tbody').each(function() {
      const link = $(this).attr('href');
      participantLinks.push(link);
    });
    participantLinks = participantLinks.filter(link => !link.includes('http'));
    
    main(participantLinks);
  })
  .catch(error => {
    console.log(error);
  });

  function timeout(fn, argument) {
    return new Promise((resolve, reject) => {
      setTimeout(async function() {
        const record = await fn(argument);
        resolve(record);
      }, 1000);
    });
  }

  async function main(links) {
    try {
      let promises = [];
      for (let i = 0; i < links.length; i++) {
        const record = getRecord(links[i]);
        promises.push(record);
      }

      const data = await Promise.all(promises);
      const csv = json2csv({ data: data, fields: config.fields });
      const fileName = `${config.name}.csv`;
      fs.writeFile(fileName, csv, (err) => {
        if (err) {
          return console.log(err);
        };
        console.log('file saved');
      });

    } catch(err) {
      console.log('error', err);
    }
  }

  function getRecord(link) {
    const options = {
      uri: `http://www.vcudmurtia.ru${link}`,
      encoding: 'binary',
      transform(body) {
        const b = new Buffer(body, 'binary');
        const result = iconv.encode(iconv.decode(b, 'win1251'), 'utf8');
        return cheerio.load(result);
      }
    };

    return request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email;
        name = $('#content').find('h4').first().text().trim();
        info = $('div', '#content').eq(1).text().trim();
        address = $('p', '#content').eq(3).find('b').text().trim();
        phone = $('p', '#content').eq(4).find('b').text().trim().replace('Тел.: ', '');
        email = $('p', '#content').eq(5).find('b').text().trim().replace('E-mail: ', '');
        site = $('p', '#content').eq(6).find('a').text().trim();

        // const s = { name, address, phone, fax, email, site, info };
        // console.log(s);
        return { name, address, phone, fax, email, site, info };
      })
      .catch(error => {
        console.log('error', error);
      });
  }


