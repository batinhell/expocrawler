const fs = require('fs');
const json2csv = require('json2csv');

const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'];

const file = fs.readFileSync('batimat.json');
const data = JSON.parse(file);
const csv = json2csv({ data, fields });
const fileName = `Batimat-rus.csv`;
fs.writeFile(fileName, csv, (err) => {
  if (err) {
    return console.log(err);
  };
  console.log('file saved');
});