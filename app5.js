const fs = require('fs');
const request = require('request-promise');
const cheerio = require('cheerio');
const json2csv = require('json2csv');

const config = {
  name: 'EMO Hannover',
  site: 'http://www.emo-hannover.de/en/exhibition/exhibitors-products/exhibitor-index/',
  fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
  linkSelector: 'a',
};

let data = [];

const options = {
  uri: config.site,
  transform(body) {
    return cheerio.load(body);
  }
};

request(options)
  .then($ => {
    let pageLinks = [];

    $('li', '.sort-results').each(function() {
      const link = $(this).find('a').attr('href');
      pageLinks.push(link);
    });
    
    mainPage(pageLinks);
    
  })
  .catch(error => {
    console.log(error);
  });
  
async function mainPage(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = await getPage(links[i]);
      promises.push(record);
    }

    const s = await Promise.all(promises);

    const csv = json2csv({ data: data, fields: config.fields });
    const fileName = `${config.name}.csv`;
    fs.writeFile(fileName, csv, (err) => {
      if (err) {
        return console.log(err);
      };
      console.log('file saved');
    });

  } catch(err) {
    console.log('error', err);
  }
}
async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = await getRecord(links[i]);
      promises.push(record);
    }
    const d = await Promise.all(promises);
    data = data.concat(d);
    console.log(data.length);

  } catch(err) {
    console.log('error', err);
  }
}


function getPage(link) {
  console.log(link);
  const options = {
    uri: `http://www.emo-hannover.de${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then(async ($) => {
      let companyLinks = [];
      $('li', '.exhibitor-list').each(function() {
        const link = $(this).find('a').attr('href');
        companyLinks.push(link);
      });
      companyLinks = companyLinks.filter(link => link);
      await main(companyLinks);
    })
    .catch(error => {
      console.log('error', error);
    });
}

function getRecord(link) {
  const options = {
    uri: `http://www.emo-hannover.de${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let name, address, phone, site, fax, info, email, selector;
      const div = $('.contact-company-block', '.flex-grid');
      name = div.find('h3[itemprop="companyName"]').text().replace(/\s\s+/g, ' ');
      const ad1 = div.find('div[itemprop="street"]').text();
      const ad2 = div.find('div[itemprop="city"]').text();
      const ad3 = div.find('div[itemprop="country"]').text();
      address = `${ad1}, ${ad2}, ${ad3}`;
      address = address.replace(/\s\s+/g, ' ');
      phone = div.find('div[itemprop="phone"]').find('a.tel-number').text().replace(/\s\s+/g, ' ');
      fax = div.find('div[itemprop="fax"]').text().replace('Fax:', '');
      site = div.find('a[itemprop="url"]').attr('href');

      
      const s = { name, address, phone, fax, email, site, info }
      // console.log(s);
      return { name, address, phone, fax, email, site, info };
    })
    .catch(error => {
      console.log('error', error);
    });
}

