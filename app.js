const fs = require('fs');
const request = require('request-promise');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const iconv = require('iconv-lite');
const puppeteer = require('puppeteer');


async function run() {
  // Открываем сайт
  browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
  const page = await browser.newPage();
  const link = 'http://www.intercharm.ru/ru/autumn/Intercharm-2017/Companies/Exhibitors/#search=startRecord%3D1025%26rpp%3D64';
  await page.goto(link, { timeout: 1000000 });
  // Ждем нужный селектор и загружаем контент
  let content, $;
  await page.waitForSelector('#gridAndList');
  content = await page.content();
  await page.waitFor(3000);
  $ = cheerio.load(content);
  // Собираем все ссылки на страницы выставок
  let participantLinks = [];
  $('.exhibitorDetail', '.exhibitor').each(function () {
    const link = $(this).find('a').attr('href');
    participantLinks.push(link);
  });
  // Обрабатываем ссылки
  main(participantLinks);
}

run();

const config = {
  name: 'intercharm',
  site: 'http://2017.osmexpo.ru/expo/?lang=ru',
  fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
};

const options = {
  uri: config.site,
  transform(body) {
    return cheerio.load(body);
  }
};


// request(options)
//   .then($ => {
//     let participantLinks = [];
//     // $('div.col-md-3', '.tx-dgm-exhibition').each(function (i, el) {
//     //   const link = $(this).find('a').first().attr('href');
//     //   participantLinks.push(link);
//     // });
//     // $('.ausstellerliste_entry', '#ausstellerliste').not('.topofthelist').each(function (i, el) {
//     //   const link = $(this).find('.col').find('a').first().attr('href');
//     //   participantLinks.push(link);
//     // });
//     // $('li.ngn-content-box', 'ul.ngn-search-list').each(function (i, el) {
//     //   const link = $(this).data('url');
//     //   participantLinks.push(link);
//     // });
//     // $('.aussteller', '.ausstellerliste').each(function (i, el) {
//     //   const link = $(this).find('a').attr('href');
//     //   participantLinks.push(link);
//     // });
//     // $('li.ef-exhlist__list-item', '.ef-exhlist__list-alpha').each(function (i, el) {
//     //   const link = $(this).find('a').first().attr('href');
//     //   participantLinks.push(link);
//     // });
//     // $('li', '.exhibitor-list').each(function() {
//     //   const link = $(this).find('a').attr('href');
//     //   participantLinks.push(link);
//     // });
//     // $('.wrapper', '.table').each(function (i, el) {
//     //   const link = $(this).attr('href');
//     //   participantLinks.push(encodeURI(link));
//     // });
//     // $('.list-group-item').each(function() {
//     //   const link = $(this).attr('href');
//     //   participantLinks.push(link);
//     // });
//     // $('ul.list').find('a').each(function() {
//     //   const link = $(this).attr('href');
//     //   participantLinks.push(link);
//     // });
//     $('.gsePartnerListItem').each(function () {
//       const link = $(this).find('a.gseHeader').attr('href');
//       participantLinks.push(link);
//     });
//     // participantLinks = participantLinks.filter((el, i, a) => i === a.indexOf(el));
//     // participantLinks = participantLinks.filter((el, i) => el.includes('http'));
//     // participantLinks = participantLinks.filter(link => link);
//     console.log(participantLinks.length);
    
//     main(participantLinks);
//   })
//   .catch(error => {
//     console.log(error);
//   });

  async function main(links) {
    try {
      let promises = [];
      // Для каждой ссылки на страницу делаем запрос и вытаскиваем данные
      for (let i = 0; i < links.length; i++) {
        const record = getRecord(links[i]);
        promises.push(record);
      }
      // Ждем пока обработаются все ссылки и сохраняем в ксв
      const data = await Promise.all(promises);
      const csv = json2csv({ data: data, fields: config.fields });

      const fileName = `${config.name}.csv`;
      fs.writeFile(fileName, csv, (err) => {
        if (err) {
          return console.log(err);
        };
        console.log('file saved');
      });

    } catch(err) {
      console.log('error', err);
    }
  }

  function getRecord(link) {
    const options = {
      uri: `http://www.intercharm.ru${link}`,
      transform(body) {
        return cheerio.load(body);
      },
    };

    return request(options)
      .then($ => {
        let name, address, phone, site, fax, info, email;

        // $('dl.contacts').children().each(function(el) {
        //   if ($(this)[0].name === 'dt') {
        //     if ($(this).text() === 'Почтовый адрес') {
        //       address = $(this).next().text();
        //     }
        //     if ($(this).text() === 'Телефон') {
        //       phone = $(this).next().text();
        //     }
        //     if ($(this).text() === 'Факс') {
        //       fax = $(this).next().text();
        //     }
        //     if ($(this).text() === 'Web') {
        //       site = $(this).next().text();
        //     }
        //     if ($(this).text() === 'E-mail') {
        //       email = $(this).next().text();
        //     }
        //   }
        // })

        name = $('h2.exhibitorName').text().trim();
        site = $('a.webAddress').attr('href');
        info = $('p.description').text().replace(/\n/g, '').replace(/\s+/g, ' ').trim();
        const ad1 = $('span.extended-address', '.adr').text();
        const ad2 = $('span.locality', '.adr').text();
        const ad3 = $('span.postal-code', '.adr').text();
        const ad4 = $('span.country-name', '.adr').text();
        address = `${ad4}, ${ad3}, ${ad2}, ${ad1}`;
        $('span', 'div.tel').each(function () {
          if ($(this).text() === 'Тел.:') {
            phone = $(this).next().text();
          }
          if ($(this).text() === 'Факс:') {
            fax = $(this).next().text();
          }
        });
        
        
        const s = { name, address, phone, fax, email, site, info };
        console.log(s);
        return { name, address, phone, fax, email, site, info };
      })
      .catch(error => {
        console.log('error', error);
      });
  }

