const fs = require('fs');
const request = require('request-promise');
const cheerio = require('cheerio');
const json2csv = require('json2csv');

const config = {
  name: 'aquatherm-moscow',
  site: 'http://www.aquatherm-moscow.ru/Aquatherm-Moscow-2018/Companies/Exhibitors/#search=rpp%3D64',
  fields: ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'],
  linkSelector: 'a',
};

let data = [];

const options = {
  uri: config.site,
  transform(body) {
    return cheerio.load(body);
  }
};

request(options)
  .then($ => {
    let alphaLinks = ['/ru/Aquatherm-Moscow-2018/Companies/Exhibitors/?rpp=12&startRecord=1'];
    $('p.blaettern').find('a').each(function () {
      const link = $(this).attr('href');
      alphaLinks.push(link);
    });
    mainLetter(alphaLinks);
    
  })
  .catch(error => {
    console.log(error);
  });

async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = await getRecord(links[i]);
      promises.push(record);
    }
    const d = await Promise.all(promises);
    data = data.concat(d);

  } catch(err) {
    console.log('error', err);
  }
}

async function mainLetter(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const page = await getPage(links[i]);
      promises.push(page);
    }
    await Promise.all(promises);

    const csv = json2csv({ data: data, fields: config.fields });
    const fileName = `${config.name}.csv`;
    fs.writeFile(fileName, csv, (err) => {
      if (err) {
        return console.log(err);
      };
      console.log('file saved');
    });

  } catch(err) {
    console.log('error', err);
  }
}



function getPage(link) {
  console.log(link);
  const options = {
    uri: `https://www.composites-europe.com${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let pageLinks = [];
      $('tbody', '.aussteller').find('tr').each(function() {
        const link = $(this).find('a').attr('href');
        pageLinks.push(link);
      });
      pageLinks = pageLinks.filter((el, i, a) => i === a.indexOf(el));
      
      return main(pageLinks);
    })
    .catch(error => {
      console.log('error', error);
    });
}


function getRecord(link) {
  const options = {
    uri: `https://www.composites-europe.com${link}`,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let name, address, phone, site, fax, info, email;
      name = $('.ausstellerbox').find('h2.ishl').text().replace(/\s\s+/g, ' ');
      const ad1 = $('.ausstellerbox').find('#box1').find('p').eq(0).text();
      const ad2 = $('.ausstellerbox').find('#box1').find('p').eq(1).text();
      const ad3 = $('.ausstellerbox').find('#box1').find('p').eq(2).text();
      address = `${ad1}, ${ad2}, ${ad3}`;

      $('.ausstellerbox').find('#box1').find('div.ind').each(function () {
        if ($(this).text() == 'Telefon:') {
          phone = $(this).next().text();
        }
        if ($(this).text() == 'Fax:') {
          fax = $(this).next().text();
        }
        if ($(this).text() == 'E-Mail:') {
          email = $(this).next().find('a').text();
        }
        if ($(this).text() == 'Webseite:') {
          site = $(this).next().find('a').attr('href');
        }
      });
      info = $('.ausstellerbox').find('#box1').find('.desc').find('p').text();

      const s = { name, address, phone, fax, email, site, info }
      // console.log(s);
      return { name, address, phone, fax, email, site, info };
    })
    .catch(error => {
      console.log('error', error);
    });
}

