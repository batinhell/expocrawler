const fs = require('fs');
const cheerio = require('cheerio');
const json2csv = require('json2csv');
const puppeteer = require('puppeteer');
const request = require('request-promise');


async function run() {
  let data = [];
  const fields = ['name', 'address', 'phone', 'fax', 'email', 'site', 'info'];

  const browser = await puppeteer.launch({ headless: false, args: ['--no-sandbox'] });
  const page = await browser.newPage();
  
  const link = 'http://www.houses-expo.ru/archive/participants/2017/';
  await page.goto(link, { timeout: 1000000 });
  let content, $;
  content = await page.content();
  $ = cheerio.load(content);

  let countItems = $('.members-list').children().length;

  while (countItems < 211) {
    await page.evaluate(_ => {
      window.scrollBy(0, window.innerHeight);
      Promise.resolve();
    });
    content = await page.content();
    $ = cheerio.load(content);
    countItems = $('.members-list').children().length;
  }

  let pageLinks = [];
  $('.item').each(function (i, el) {
    const link = $(this).find('a.name').attr('href');
    pageLinks.push(link);
  });
  pageLinks = pageLinks.filter(link => link);
  console.log(pageLinks.length);
  
  const t = await main(pageLinks);
  data = data.concat(t);

  console.log('end');

  const csv = json2csv({ data, fields });
  const fileName = `Деревянный дом – 2017.csv`;
  fs.writeFile(fileName, csv, (err) => {
    if (err) {
      return console.log(err);
    };
    console.log('file saved');
  });


  browser.close();
}

run();

async function main(links) {
  try {
    let promises = [];
    for (let i = 0; i < links.length; i++) {
      const record = getRecord(links[i]);
      promises.push(record);
    }
    const d = await Promise.all(promises);
    return d;

  } catch (err) {
    console.log('error', err);
  }
}

function getRecord(link) {
  const options = {
    uri: link,
    transform(body) {
      return cheerio.load(body);
    },
  };

  return request(options)
    .then($ => {
      let name, address, phone, site, fax, info, email;
      name = $('.content').find('h1').first().text();
      address = $('.content').find('.participant__location').text().replace(/\n/g, '').replace(/\t/g, '').trim();
      info = $('.content').find('.text').text().replace(/\n/g, '').replace(/\t/g, '').trim();
      site = $('.content').find('a.name').attr('href');

      const s = { name, address, phone, fax, email, site, info };
      console.log(s);
      return { name, address, phone, fax, email, site, info };
    })
    .catch(error => {
      console.log('error', error);
    });
}

